package fr.financeactive.taxe;

import java.math.BigDecimal;

/**
 * Interface which describe the behavior of a taxable product
 *  
 * @author jgrenier
 * @version 1.0
 */
public interface TaxableProduct {
	
	public static final BigDecimal TAXE_EXEMPTED = BigDecimal.valueOf(0.0);
	
	public static final BigDecimal TAXE_10_PERCENT = BigDecimal.valueOf(10.0);
	
	public static final BigDecimal TAXE_5_PERCENT = BigDecimal.valueOf(5.0);
	
	/**
	 * Returns the applicable taxe.
	 * @return {@link BigDecimal}
	 */
	public BigDecimal getTaxeApplicable();
}
