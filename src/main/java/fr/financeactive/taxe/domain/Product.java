package fr.financeactive.taxe.domain;

import java.math.BigDecimal;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.lang.NonNull;

import fr.financeactive.taxe.ProductCategory;

/*
 * @Author Julien Grenier
 * @version
 * 
 * 
 */
@Configurable
public class Product {
	
	
	
	/** Product Name */
	@NonNull
	private String name = "";
	
	/** Price without taxe */
	@NonNull
	private BigDecimal priceHt;
	
	private BigDecimal priceTTC;
	
	private BigDecimal taxeImputed;
	
	/** Indicates wether the product is imported or not */
	@NonNull
	private Boolean imported;
	
	/** Specifying the category of the product in order to know taxe applicability */
	@NonNull
	private ProductCategory productCategory;
	
	public Product() {
		this.name = "Product Name not initialized";
		this.priceHt = BigDecimal.valueOf(0);
		this.productCategory = ProductCategory.NULL;	
		this.imported = Boolean.FALSE;
		this.taxeImputed = BigDecimal.valueOf(0);
		this.priceTTC = BigDecimal.valueOf(0);
	}
	
	public Product(String name, BigDecimal priceHt, Boolean isImported, ProductCategory productCategory) {
		this.name = name;
		this.priceHt = priceHt;
		this.productCategory = productCategory;	
		this.imported = isImported;
		
		
	}

	

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the priceHt
	 */
	public BigDecimal getPriceHt() {
		return priceHt;
	}

	/**
	 * @param priceHt the priceHt to set
	 */
	public void setPriceHt(BigDecimal priceHt) {
		this.priceHt = priceHt;
	}

	/**
	 * @return the priceTTC
	 */
	public BigDecimal getPriceTTC() {
		return priceTTC;
	}

	/**
	 * @param priceTTC the priceTTC to set
	 */
	public void setPriceTTC(BigDecimal priceTTC) {
		this.priceTTC = priceTTC;
	}

	/**
	 * @return the taxeImputed
	 */
	public BigDecimal getTaxeImputed() {
		return taxeImputed;
	}

	/**
	 * @param taxeImputed the taxeImputed to set
	 */
	public void setTaxeImputed(BigDecimal taxeImputed) {
		this.taxeImputed = taxeImputed;
		this.priceTTC = BigDecimal.valueOf(0).add(this.priceHt).add(taxeImputed);
	}

	/**
	 * @return the imported
	 */
	public Boolean isImported() {
		return imported;
	}

	/**
	 * @param imported the imported to set
	 */
	public void setImported(Boolean imported) {
		this.imported = imported;
	}

	/**
	 * @return the productCategory
	 */
	public ProductCategory getProductCategory() {
		return productCategory;
	}

	/**
	 * @param productCategory the productCategory to set
	 */
	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name,priceHt);
	}

	@Override
	public boolean equals(Object obj) {
		return Objects.equals(this,  obj);
	}

	/**
	 * returns a String which describe the object
	 */
	@Override
	public String toString() {
		return "Product [ name=" + name + ", priceHt=" + priceHt + ", priceTTC="
				+ priceTTC + ", taxeImputed=" + taxeImputed + ", imported=" + imported + ", productCategory="
				+ productCategory + "]";
	}

	/**
	 * Returns (an invoice) string representation of the object.
	 * @return a String
	 */
	public String print() {
		return new StringBuilder("1 ").append(this.name)
				.append(" : ").append(this.priceTTC)
				.toString();
	}
	
	
}
