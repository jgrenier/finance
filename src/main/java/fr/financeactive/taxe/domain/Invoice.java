package fr.financeactive.taxe.domain;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Represents an invoice.
 * 
 * @author jgrenier
 * @version 1.0
 */
public class Invoice {

	/** invoice name */
	private String name;

	/** List of invoice products */
	private Collection<Product> products;

	public Invoice(String name) {
		this.name = name;
		products = new ArrayList<Product>();
	}

	/** fluent constructor to add product */
	public Invoice withProduct(Product product) {
		this.products.add(product);
		return this;
	}

	/**
	 * @return the products
	 */
	public Collection<Product> getProducts() {
		return products;
	}

	/**
	 * @param products
	 *            the products to set
	 */
	public void setProducts(Collection<Product> products) {
		this.products = products;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
