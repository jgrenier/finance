package fr.financeactive.taxe;

import java.math.BigDecimal;

/**
 * Enumeration implementing the {@link TaxableProduct} interface
 * 
 * @author jgrenier
 * @version 1.0
 */
public enum ProductCategory implements TaxableProduct {

	NULL(TaxableProduct.TAXE_EXEMPTED),
	
	BOOK(TaxableProduct.TAXE_EXEMPTED),
	
	FOOD(TaxableProduct.TAXE_EXEMPTED),
	
	MEDICAMENT(TaxableProduct.TAXE_EXEMPTED),
	
	OTHER(TaxableProduct.TAXE_10_PERCENT);
	
	private BigDecimal taxeApplicable;
	
	ProductCategory(BigDecimal taxeApplicable) {
		this.taxeApplicable = taxeApplicable;
	}
	
	public BigDecimal getTaxeApplicable() {
		return this.taxeApplicable;
	}
	
	
	
}
