package fr.financeactive.taxe.service;

import fr.financeactive.taxe.domain.Invoice;

/**
 * Interface of the invoice service
 * @author jgrenier
 * @version 1.0
 */
public interface InvoiceService {

	/**
	 * Print the detailed Invoice
	 * @param invoice
	 */
	public String printInvoice(Invoice invoice);
	
}
