package fr.financeactive.taxe.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.financeactive.taxe.ProductCategory;
import fr.financeactive.taxe.TaxeCalculator;
import fr.financeactive.taxe.domain.Product;

/**
 * A service utility to manage products.
 * 
 * @author jgrenier
 * @version 1.0
 */
@Service
public class ProductService {

	@Autowired
	private TaxeCalculator taxeCalculator;

	/**
	 * Creates and initializes a product.
	 * 
	 * @param name
	 *            the name of the product
	 * @param price
	 *            his price without any taxe
	 * @param imported
	 *            indicates if the product is imported or not
	 * @param productCategory
	 *            the type of product, will be used to know the taxe amount
	 * @return A Product instance initialized.
	 */
	public Product createProduct(String name, double price, Boolean imported, ProductCategory productCategory) {
		Product product = new Product(name, BigDecimal.valueOf(price), imported, productCategory);
		product.setTaxeImputed(taxeCalculator.getImputedTaxe(product));
		return product;
	}

}
