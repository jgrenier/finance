package fr.financeactive.taxe.service;

import java.math.BigDecimal;
import java.util.Collection;

import org.springframework.stereotype.Service;

import fr.financeactive.taxe.domain.Invoice;
import fr.financeactive.taxe.domain.Product;

import static java.util.stream.Collectors.joining;

/**
 * Service Class to manage invoices
 * @author jgrenier
 * @version 1.0
 */
@Service 
public class InvoiceServiceImpl implements InvoiceService {

	/**
	 * Method for printing the invoice on the screen
	 * @param Invoice the {@link Invoice} object
	 * @return The string to display
	 */
	public String printInvoice(Invoice invoice) {
		return new StringBuilder()
				.append("###### Invoice ").append(invoice.getName()).append("######\n")
				.append(invoice.getProducts().stream().map(Product::print).collect(joining("\n"))).append("\n")
				.append("Montant des taxes : ").append(getInvoiceTotalTaxe(invoice.getProducts())).append("\n")
				.append("Total : ").append(getInvoiceTotalTTC(invoice.getProducts()))					
				.toString();
	}

	/**
	 * Returns the sum of the taxe applied on all the products contained in the invoice.
	 * @param products a {@link Collection}<{@link Product}>
	 * @return {@link BigDecimal}
	 */
	private BigDecimal getInvoiceTotalTaxe(Collection<Product> products) {
		return products.stream().map( Product::getTaxeImputed)
					.reduce(BigDecimal::add).get();
	}
	
	/**
	 * Returns the sum of the priceTTC of all the products contained in the invoice.
	 * @param products a {@link Collection}<{@link Product}>
	 * @return {@link BigDecimal} 
	 */
	private BigDecimal getInvoiceTotalTTC(Collection<Product> products) {
		return products.stream().map( Product::getPriceTTC)
				.reduce(BigDecimal::add).get();
	}
	
}
