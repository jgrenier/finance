package fr.financeactive.taxe;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Component;

import fr.financeactive.taxe.domain.Product;

/**
 * TaxeCalculator : Utility class to calculate the taxe amount
 * 
 * @author jgrenier
 * @version 1.0
 */
@Component("taxeCalculator")
public class TaxeCalculator {

	/** Define the precision of the round operations. here at 5 cent (euros) SUP */
	private static final BigDecimal ROUND_PRECISION = BigDecimal.valueOf(0.05);

	/**
	 * Returns the real taxe to be applied.
	 * 
	 * @param product
	 *            the target {@link Product} to be calculated.
	 * @return the amount of taxe
	 */
	public BigDecimal getImputedTaxe(Product product) {

		BigDecimal result = getCalculatedTaxe(product.getPriceHt(), product.getProductCategory().getTaxeApplicable());

		if (product.isImported()) {
			result = result.add(getCalculatedTaxe(product.getPriceHt(), TaxableProduct.TAXE_5_PERCENT));
		}
		return result;
	}

	/**
	 * Calculate the taxe and apply a round operation to {@link ROUND_PRECISION}
	 * 
	 * @param priceHt
	 *            the base price used to calculate
	 * @param applicableTaxe
	 *            the percentage of taxe to caclulate
	 * @return the value of the taxe calculated
	 */
	private BigDecimal getCalculatedTaxe(BigDecimal priceHt, BigDecimal applicableTaxe) {

		BigDecimal priceTTC = priceHt.multiply(applicableTaxe).divide(BigDecimal.valueOf(100)).setScale(2,
				RoundingMode.CEILING);

		BigDecimal divisionRemainder = priceTTC.remainder(ROUND_PRECISION);

		if (divisionRemainder.doubleValue() > 0) {
			priceTTC = priceTTC.add(ROUND_PRECISION.subtract(divisionRemainder));
		}

		return priceTTC;
	}

}
