package fr.financeactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main application.
 * 
 * @author jgrenier
 * @version 1.O
 * @see {@link InvoiceRunner}
 */
@SpringBootApplication
public class FinanceactiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinanceactiveApplication.class, args);
	}
}
