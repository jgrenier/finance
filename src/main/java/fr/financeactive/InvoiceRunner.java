package fr.financeactive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import fr.financeactive.taxe.ProductCategory;
import fr.financeactive.taxe.domain.Invoice;
import fr.financeactive.taxe.service.InvoiceService;
import fr.financeactive.taxe.service.ProductService;

/**
 * Main scenario
 * @author jgrenier
 *
 */
@Component
public class InvoiceRunner implements CommandLineRunner {

	@Autowired
	public InvoiceService invoiceService;

	@Autowired
	public ProductService productService;

	@Override
	public void run(String... args) throws Exception {

		Invoice invoice1 = new Invoice("Input 1")
				.withProduct(productService.createProduct("Livre", 12.49, Boolean.FALSE, ProductCategory.BOOK))
				.withProduct(productService.createProduct("CD Musical", 14.99, Boolean.FALSE, ProductCategory.OTHER))
				.withProduct(
						productService.createProduct("Barre de chocolat", 0.85, Boolean.FALSE, ProductCategory.FOOD));

		System.out.println(invoiceService.printInvoice(invoice1));

		Invoice invoice2 = new Invoice("Input 2")
				.withProduct(productService.createProduct("boîte de chocolats importée", 10.00, Boolean.TRUE,
						ProductCategory.FOOD))
				.withProduct(productService.createProduct("flacon de parfum importé", 47.50, Boolean.TRUE,
						ProductCategory.OTHER));

		System.out.println(invoiceService.printInvoice(invoice2));

		Invoice invoice3 = new Invoice("Input 3")
				.withProduct(productService.createProduct("flacon de parfum importé", 27.99, Boolean.TRUE,
						ProductCategory.OTHER))
				.withProduct(
						productService.createProduct("flacon de parfum", 18.99, Boolean.FALSE, ProductCategory.OTHER))
				.withProduct(productService.createProduct("boîte de pilules contre la migraine", 9.75, Boolean.FALSE,
						ProductCategory.MEDICAMENT))

				.withProduct(productService.createProduct("boîte de chocolats importés", 11.25, Boolean.TRUE,
						ProductCategory.FOOD));

		System.out.println(invoiceService.printInvoice(invoice3));

	}
}
